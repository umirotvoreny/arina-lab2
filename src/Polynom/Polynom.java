package Polynom;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

public final class Polynom {
    private static final double PRECISION = 0.001;

    private final ArrayList<Double> coefficients;

    public int getPow() {
        return coefficients.size() - 1;
    }

    private void cleanUp() {
        for (int i = coefficients.size() - 1; i > 0; --i) {
            if (Math.abs(coefficients.get(i)) < PRECISION) {
                coefficients.remove(i);
            } else {
                break;
            }
        }
    }

    public Polynom() {
        coefficients = new ArrayList<>(List.of(1.0));
    }

    public Polynom(ArrayList<Double> initializeCoefficients) {
        Objects.requireNonNull(initializeCoefficients);
        if (initializeCoefficients.size() == 0) {
            coefficients = new ArrayList<>(List.of(1.0));
        } else {
            coefficients = new ArrayList<>(initializeCoefficients);
        }
        cleanUp();
    }

    public Polynom(int pow) {
        coefficients = new ArrayList<>();
        var random = new Random();
        for (int i = 0; i <= pow + 1; ++i) {
            coefficients.add(random.nextDouble(10) - 5);
        }

        if (coefficients.get(pow) < PRECISION) {
            coefficients.set(pow, 1.0);
        }
    }

    double calcInPoint(double x) {
        double result = coefficients.get(0);
        for (int i = 1; i < coefficients.size(); ++i) {
            result += coefficients.get(i) * Math.pow(x, i);
        }
        return result;
    }

    public Polynom setCoefficient(int pow, double coefficient) {
        if (coefficient < 0) {
            throw new IllegalArgumentException();
        }

        while (coefficients.size() <= pow) {
            coefficients.add(0.0);
        }

        coefficients.set(pow, coefficient);
        cleanUp();
        return this;
    }

    public Double getCoefficient(int pow) {
        if (pow > this.getPow()) {
            throw new IllegalArgumentException();
        }
        return coefficients.get(pow);
    }

    public Polynom multiply(double constant) {
        var newPolyArrayList = new ArrayList<>(coefficients);
        newPolyArrayList.replaceAll(aDouble -> aDouble * constant);
        return new Polynom(newPolyArrayList);
    }

    public Polynom multiply(Polynom polynom) {
        Objects.requireNonNull(polynom);
        var newPolyArrayList = new ArrayList<Double>();
        for (int i = 0; i <= this.getPow() + polynom.getPow(); ++i) {
            newPolyArrayList.add(0.0);
        }

        for (int i = 0; i <= this.getPow(); ++i) {
            for (int j = 0; j <= polynom.getPow(); ++j) {
                newPolyArrayList.set(i + j, newPolyArrayList.get(i + j) + (this.getCoefficient(i) * polynom.getCoefficient(j)));
            }
        }

        return new Polynom(newPolyArrayList);
    }

    public Polynom plus(double constant) {
        var newPolyArrayList = new ArrayList<>(coefficients);
        newPolyArrayList.set(0, coefficients.get(0) + constant);
        return new Polynom(newPolyArrayList);
    }

    public Polynom plus(Polynom polynom) {
        Objects.requireNonNull(polynom);
        var newPolyArrayList = new ArrayList<>(coefficients);
        while (newPolyArrayList.size() < polynom.getPow() + 1) {
            newPolyArrayList.add(0.0);
        }

        for (int i = 0; i < polynom.getPow() + 1; ++i) {
            newPolyArrayList.set(i, newPolyArrayList.get(i) + polynom.getCoefficient(i));
        }

        return new Polynom(newPolyArrayList);
    }

    public Polynom derivative() {
        var newPolyArrayList = new ArrayList<>(coefficients);
        for (int i = 0; i < coefficients.size() - 1; ++i) {
            newPolyArrayList.set(i, coefficients.get(i + 1) * (i + 1));
        }

        if (coefficients.size() == 1) {
            newPolyArrayList.set(0, 0.0);
        } else {
            newPolyArrayList.remove(newPolyArrayList.size() - 1);
        }

        return new Polynom(newPolyArrayList);
    }

    public Polynom antiderivative(double constant) {
        var newPolyArrayList = new ArrayList<Double>();
        newPolyArrayList.add(constant);
        for (int i = 1; i < coefficients.size() + 1; ++i) {
            newPolyArrayList.add(coefficients.get(i - 1) / i);
        }

        return new Polynom(newPolyArrayList);
    }

    public Double calcDefiniteIntegral(double from, double to) {
        var antiderivative = this.antiderivative(0);
        return antiderivative.calcInPoint(to) - antiderivative.calcInPoint(from);
    }

    @Override
    public String toString() {
        var sb = new StringBuilder(coefficients.get(0).toString());
        for(int i = 1; i < coefficients.size(); ++i) {
            sb.append(" ").append(coefficients.get(i));
        }
        return sb.toString();
    }
}
