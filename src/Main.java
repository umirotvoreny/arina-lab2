import Polynom.Polynom;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        var poly = new Polynom(new ArrayList<>(List.of(1.0, 2.0, 3.0, 4.0)));
        var poly2 = new Polynom(new ArrayList<>(List.of(5.0,6.0)));
        System.out.println(poly);
        System.out.println(poly.plus(1));
        System.out.println(poly.plus(poly2));
        System.out.println(poly.multiply(3));
        System.out.println(poly.multiply(poly2));
        System.out.println(poly.derivative());
        System.out.println(poly.antiderivative(4));
        System.out.println(poly.calcDefiniteIntegral(2, 7));

        System.out.println(poly.multiply(new Polynom(new ArrayList<>(List.of(0.0)))));

        var poly3 = new Polynom(new ArrayList<>(List.of(4.2,5.3)));
        poly3.setCoefficient(6, 1.1);
        System.out.println(poly3);
        poly3.setCoefficient(6, 0);
        System.out.println(poly3);
    }
}